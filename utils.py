import pandas as pd
from scipy.signal import find_peaks

def infinite_sequence():
    """
    Generates a infinite sequence counting from zero.
    """
    num = 0
    while True:
        yield num
        num += 1


def get_peaks_dataframe(time_series, peaks, properties):
    """
    Obtains start-, maximum-, and stop time of a peak and stacks them to a dataframe.
    """
    indices_peak = time_series.index[peaks]
    detected_peaks = time_series.loc[indices_peak]

    indices_left_ip = time_series.index[properties['left_ips'].astype(int)]
    left_ip = time_series.loc[indices_left_ip]
    left_ip['left_ip_time'] = indices_left_ip
    left_ip.index = detected_peaks.index

    indices_right_ip = time_series.index[properties['right_ips'].astype(int)]
    right_ip = time_series.loc[indices_right_ip]
    right_ip['right_ip_time'] = indices_right_ip
    right_ip.index = detected_peaks.index

    df = pd.DataFrame(data={
        'peaks': detected_peaks.index,
        'peaks_value': detected_peaks['contaminant'],
        'left_ip': left_ip['left_ip_time'],
        'left_ip_value': left_ip['contaminant'],
        'right_ip': right_ip['right_ip_time'],
        'right_ip_value': right_ip['contaminant']
    },
    )
    return df


def collect_target_signals(df, target_signal):
    """
    Collect target events within a detected peak.
    """
    stacked_df = list()
    for index, row in df.iterrows():
        temp_df = target_signal[(target_signal.index > row['left_ip']) & (target_signal.index < row['right_ip'])]
        stacked_df.append(temp_df)
    return stacked_df


def get_events(target_signal):
    """
    Returns dictionary with starts and ends of a web_break.
    """
    line_width=5
    shapes = list()
    for index, row in target_signal.iterrows():
        if row['label'] == 1:
            color = 'red'
        else:
            color = 'green'
        shapes.append(
            dict(
                type='rect',
                x0=index - line_width,
                y0=-5,
                x1=index + line_width,
                y1=5,
                fillcolor=color,
                opacity=1,
                layer='below',
                line_width=0,
            )
        )
    return shapes

def get_detected_events(df, target_signal):
    stacked_df = list()
    no_events_occured = 0
    for index, row in df.iterrows():
        temp_df = target_signal[(target_signal.index > row['left_ip']) & (target_signal.index < row['right_ip'])]
        if not temp_df.empty:
            stacked_df.append(temp_df)
        else:
            no_events_occured + 1
    return pd.concat(stacked_df), no_events_occured


def get_score(peaks, true_peaks):
    true_detected_peaks = [p for p in peaks if p in true_peaks]

    true_peaks_count = len(true_peaks)
    tdp_count = len(true_detected_peaks)
    peaks_count = len(peaks)
    if (tdp_count == 0) or (peaks_count == 0):
        score = 0
    else:
        precision = tdp_count / peaks_count
        recall = tdp_count / true_peaks_count
        score = precision * recall
    return score

def run_grid_search(time_series_data, target_signal, *, prominence, width, distance):
    best_results = list([0])
    counter = infinite_sequence()

    true_peaks = target_signal.query('label == 1').index

    for p in prominence:
        for w in width:
            for d in distance:
                print(f'Number of iteration: {next(counter)}')

                peak_detection_parameters = dict(
                    prominence=p,
                    width=w,
                    distance=d,

                )
                peaks, properties = find_peaks(time_series_data, **peak_detection_parameters)
                score = get_score(peaks, true_peaks)

                # Evaluation
                if score > best_results[-1]:
                    best_results.append(score)
                    best_config = peak_detection_parameters

    return best_results, best_config

def calc_performance(df, target_signal):
    detected_events, no_events_occured = get_detected_events(df, target_signal)
    events_in_timeframe = target_signal[target_signal['label'] == 1]

    count_detected_events = detected_events[detected_events['label'] == 1].shape[0]
    count_true_events = events_in_timeframe.shape[0]

    true_positive_rate = count_detected_events / count_true_events

    falsely_predicted_events = detected_events[detected_events['label'] == 0].shape[0] + 1

    return true_positive_rate / (falsely_predicted_events + no_events_occured)
